export interface Intern {
    id?: string;
    firstname: string;
    lastname: string;
    age: number;
    dateofbirth:string;
}
