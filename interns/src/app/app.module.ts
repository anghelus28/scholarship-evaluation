import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HomeModuleModule } from './home-module/home-module.module';

@NgModule({
  declarations: [
  ],
  imports: [
    HomeModuleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
