import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InternService } from '../services/intern.service';

@Component({
  selector: 'app-add-intern',
  templateUrl: './add-intern.component.html',
  styleUrls: ['./add-intern.component.scss']
})
export class AddInternComponent implements OnInit {

  firstname: string;
  lastname: string;
  age: number;
  dateofbirth : string;

  constructor(private _router: Router, private _activatedRoute: ActivatedRoute, private service: InternService) { }

  ngOnInit(): void {
    this._activatedRoute.params.subscribe(parameter =>
      (
        console.log(parameter['id'])
      ))
  }

  add() {
    var y: number = +this.age;
    this.service.addIntern(this.firstname,this.lastname,y ,this.dateofbirth).subscribe();
  }
}
