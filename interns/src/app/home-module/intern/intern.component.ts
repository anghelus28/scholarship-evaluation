import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Intern } from '../../intern';
import { InternService } from '../services/intern.service';

@Component({
  selector: 'app-intern',
  templateUrl: './intern.component.html',
  styleUrls: ['./intern.component.scss']
})
export class InternComponent implements OnInit,OnChanges {

  @Input() searchedText:string;

  interns:Intern[];

  constructor ( private internService: InternService,private dialog: MatDialog){
    
  }
  ngOnChanges(): void {
    this.internService.getInterns().subscribe((interns:Intern[])=> {this.interns=interns});
  }

  ngOnInit(): void {
    this.internService.getInterns().subscribe((interns:Intern[])=> {this.interns=interns});
  }

  public DeleteIntern(target:string) {
    this.internService.deleteIntern(target).subscribe();
    this.ngOnChanges();
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(InternComponent, {
      width: '250px',
      data: { name:"txt" }
    });
  }
}

