import { Injectable } from '@angular/core';
import { Intern } from 'src/app/intern';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InternService {

  readonly baseUrl= "https://localhost:44317/interns";

  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private httpClient: HttpClient) { }

  addIntern(inputFirstName: string, inputLastName:string, inputAge:number,inputDateOfBirth:string) {

    let intern = { 
      firstname: inputFirstName,
      lastname: inputLastName,
      age: inputAge,
      dateofbirth:inputDateOfBirth
    }
    return this.httpClient.post(this.baseUrl, intern, this.httpOptions);
  }

  deleteIntern(inputID:string)
  {
    return this.httpClient.delete(this.baseUrl  + inputID, this.httpOptions);
  }
  getInterns():Observable<Intern[]> {
    return this.httpClient.get<Intern[]>(this.baseUrl, this.httpOptions);
  }
}

