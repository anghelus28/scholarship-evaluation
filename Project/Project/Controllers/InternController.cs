﻿using Microsoft.AspNetCore.Mvc;
using Project.Models;
using Project.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class InternController : ControllerBase
    {
        IInternCollectionService _internCollectionService;
        public InternController(IInternCollectionService internCollectionService)
        {
            _internCollectionService = internCollectionService ?? throw new ArgumentNullException(nameof(internCollectionService));
        }

        [HttpGet]

        public async Task<IActionResult> GetInterns()
        {
            return Ok(await _internCollectionService.GetAll());
        }

        [HttpPost]

        public async Task<IActionResult> CreateIntern([FromBody] Intern intern)
        {
            if (intern.Id == Guid.Empty)
                intern.Id = Guid.NewGuid();
            if (intern == null)
                return BadRequest("Intern is null");
            await _internCollectionService.Create(intern);
            return Ok(await _internCollectionService.GetAll());
        }

        [HttpPut("{id}")]

        public async Task<IActionResult> UpdateIntern(Guid id, [FromBody] Intern intern)
        {
            await _internCollectionService.Update(id, intern);
            return Ok(await _internCollectionService.GetAll());
        }

        [HttpDelete("{id}")]

        public async Task<IActionResult> DeleteIntern(Guid id)
        {
            await _internCollectionService.Delete(id);
            return Ok(await _internCollectionService.GetAll());
        }

        [HttpGet("{id}")]

        public async Task<IActionResult> GetInternsById(Guid id)
        {
            await _internCollectionService.Get(id);
            return Ok(await _internCollectionService.GetAll());
        }
    }

}
